Pod::Spec.new do |s|
  s.name             = 'Tanker'
  s.version          = '0.1.0'
  s.summary          = 'Cocoapods Spec placeholder'

  s.description      = <<-DESC
Simple package to reserve the name Tanker on cocoapods.org
                       DESC

  s.homepage         = 'https://tanker.io'
  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.author           = { 'Tanker tech team' => 'tech@tanker.io' }
  s.source           = { :git => 'https://gitlab.com/TankerHQ/cocoapods-spec-placeholder.git', :tag => s.version.to_s }

  s.ios.deployment_target = '9.0'

  s.source_files = 'Tanker/Tanker/Classes/**/*'
end
